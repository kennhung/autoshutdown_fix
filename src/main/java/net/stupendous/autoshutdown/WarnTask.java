/*    */
package net.stupendous.autoshutdown;

/*    */
/*    */ import java.util.TimerTask;
/*    */ import java.util.concurrent.TimeUnit;
/*    */ import net.stupendous.autoshutdown.misc.Log;
/*    */ import net.stupendous.autoshutdown.misc.Util;
/*    */ import org.bukkit.plugin.Plugin;

/*    */
/*    */ public class WarnTask extends TimerTask {
    /*    */ protected final AutoShutdownPlugin plugin;
    /*    */ protected final Log log;
    /* 12 */ protected long seconds = 0L;

    /*    */
    /*    */ public WarnTask(AutoShutdownPlugin plugin, long seconds) {
        /* 15 */ this.plugin = plugin;
        /* 16 */ this.log = plugin.log;
        /* 17 */ this.seconds = seconds;
        /*    */ }

    /*    */
    /*    */ public void run() {
        /* 21 */ this.plugin.getServer().getScheduler().scheduleSyncDelayedTask((Plugin) this.plugin, new Runnable() {
            /*    */ public void run() {
                /* 23 */ if (TimeUnit.SECONDS.toMinutes(WarnTask.this.seconds) > 0L) {
                    /* 24 */ if (TimeUnit.SECONDS.toMinutes(WarnTask.this.seconds) == 1L) {
                        /* 25 */ if (WarnTask.this.seconds
                                - TimeUnit.MINUTES.toSeconds(TimeUnit.SECONDS.toMinutes(WarnTask.this.seconds)) == 0L) {
                            /* 26 */ Util.broadcast(String
                                    .valueOf(WarnTask.this.plugin.settings.config.getString("messages.shutdownmessage"))
                                    + " in 1 " +
                            /* 27 */ WarnTask.this.plugin.settings.config.getString("messages.minute") + "...");
                            /*    */ } else {
                            /* 29 */ Util.broadcast(/* 30 */ String
                                    .valueOf(WarnTask.this.plugin.settings.config.getString("messages.shutdownmessage"))
                                    + " in 1 " +
                            /* 31 */ WarnTask.this.plugin.settings.config.getString("messages.minute") + " %d " +
                            /* 32 */ WarnTask.this.plugin.settings.config.getString("messages.second") + "s ...",
                                    /* 33 */ new Object[] {
                                            /* 34 */ Long.valueOf(WarnTask.this.seconds - TimeUnit.MINUTES
                                                    .toSeconds(TimeUnit.SECONDS.toMinutes(WarnTask.this.seconds)))
                                    /*    */ });
                            /*    */ }
                        /* 37 */ } else if (WarnTask.this.seconds
                                - TimeUnit.MINUTES.toSeconds(TimeUnit.SECONDS.toMinutes(WarnTask.this.seconds)) == 0L) {
                        /* 38 */ Util.broadcast(String
                                .valueOf(WarnTask.this.plugin.settings.config.getString("messages.shutdownmessage"))
                                + " in %d " +
                        /* 39 */ WarnTask.this.plugin.settings.config.getString("messages.minute") + "s ...",
                                /* 40 */ new Object[] {
                                        Long.valueOf(TimeUnit.SECONDS.toMinutes(WarnTask.this.seconds)) });
                        /*    */ } else {
                        /* 42 */ Util.broadcast(/* 43 */ String
                                .valueOf(WarnTask.this.plugin.settings.config.getString("messages.shutdownmessage"))
                                + " in %d " +
                        /* 44 */ WarnTask.this.plugin.settings.config.getString("messages.minute") + "s %d " +
                        /* 45 */ WarnTask.this.plugin.settings.config.getString("messages.second") + "s ...",
                                /* 46 */ new Object[] {
                                        /* 47 */ Long.valueOf(TimeUnit.SECONDS.toMinutes(WarnTask.this.seconds)),
                                        /* 48 */ Long.valueOf(WarnTask.this.seconds -
                                /* 49 */ TimeUnit.MINUTES.toSeconds(TimeUnit.SECONDS.toMinutes(WarnTask.this.seconds)))
                                /*    */ });
                        /*    */ }
                    /* 52 */ } else if (TimeUnit.SECONDS.toSeconds(WarnTask.this.seconds) == 1L) {
                    /* 53 */ Util.broadcast(
                            String.valueOf(WarnTask.this.plugin.settings.config.getString("messages.shutdownmessage"))
                                    + " NOW!");
                    /*    */ } else {
                    /* 55 */ Util.broadcast(
                            String.valueOf(WarnTask.this.plugin.settings.config.getString("messages.shutdownmessage"))
                                    + " in %d " +
                    /* 56 */ WarnTask.this.plugin.settings.config.getString("messages.second") + "s ...",
                            /* 57 */ new Object[] { Long.valueOf(WarnTask.this.seconds) });
                    /*    */ }
                /*    */ }
            /*    */ });
        /*    */ }
    /*    */ }

/*
 * Location: D:\New folder
 * (2)\AutoShutdown.jar!\net\stupendous\autoshutdown\WarnTask.class Java
 * compiler version: 7 (51.0) JD-Core Version: 1.1.3
 */