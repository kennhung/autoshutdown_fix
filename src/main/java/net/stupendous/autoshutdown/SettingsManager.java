/*    */ package net.stupendous.autoshutdown;
/*    */ 
/*    */ import java.io.File;
/*    */ import java.io.IOException;
/*    */ import org.bukkit.Bukkit;
/*    */ import org.bukkit.ChatColor;
/*    */ import org.bukkit.configuration.file.FileConfiguration;
/*    */ import org.bukkit.configuration.file.YamlConfiguration;
/*    */ import org.bukkit.plugin.Plugin;
/*    */ import org.bukkit.plugin.PluginDescriptionFile;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class SettingsManager
/*    */ {
/*    */   Plugin p;
/*    */   FileConfiguration config;
/*    */   File cfile;
/* 22 */   static SettingsManager instance = new SettingsManager();
/*    */   
/*    */   public static SettingsManager getInstance() {
/* 25 */     return instance;
/*    */   }
/*    */   
/*    */   public void setup(Plugin p) {
/* 29 */     this.p = p;
/* 30 */     this.config = p.getConfig();
/* 31 */     p.saveDefaultConfig();
/* 32 */     this.cfile = new File(p.getDataFolder(), "config.yml");
/*    */   }
/*    */   
/*    */   public FileConfiguration getConfig() {
/* 36 */     return this.config;
/*    */   }
/*    */ 
/*    */   
/*    */   public void saveConfig() {
/*    */     try {
/* 42 */       this.config.save(this.cfile);
/* 43 */     } catch (IOException e) {
/*    */       
/* 45 */       Bukkit.getServer().getLogger().severe(ChatColor.RED + "Unable to save configuration.");
/*    */     } 
/*    */   }
/*    */   
/*    */   public void reloadConfig() {
/* 50 */     if (this.cfile.exists()) {
/* 51 */       this.config = (FileConfiguration)YamlConfiguration.loadConfiguration(this.cfile);
/*    */     } else {
/* 53 */       this.p.saveDefaultConfig();
/*    */     } 
/*    */   }
/*    */   public PluginDescriptionFile getDesc() {
/* 57 */     return this.p.getDescription();
/*    */   }
/*    */ }


/* Location:              D:\New folder (2)\AutoShutdown.jar!\net\stupendous\autoshutdown\SettingsManager.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       1.1.3
 */