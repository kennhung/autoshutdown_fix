/*     */
package net.stupendous.autoshutdown;

/*     */
/*     */ import java.util.ArrayList;
/*     */ import java.util.Calendar;
import java.util.Collection;
/*     */ import java.util.Timer;
/*     */ import java.util.TreeSet;
/*     */ import net.stupendous.autoshutdown.misc.Log;
/*     */ import net.stupendous.autoshutdown.misc.Util;
/*     */ import org.bukkit.command.CommandExecutor;
/*     */ import org.bukkit.entity.Player;
/*     */ import org.bukkit.plugin.Plugin;
/*     */ import org.bukkit.plugin.java.JavaPlugin;
/*     */ import org.bukkit.scheduler.BukkitScheduler;

/*     */
/*     */ public class AutoShutdownPlugin/*     */ extends JavaPlugin {
    /*     */ public String pluginName;
    /*     */ public Log log;
    /* 19 */ protected ShutdownScheduleTask task = null;
    /* 20 */ protected Timer backgroundTimer = null;
    /* 21 */ protected Timer shutdownTimer = null;
    /* 22 */ protected BukkitScheduler scheduler = null;
    /*     */ protected boolean shutdownImminent = false;
    /* 24 */ protected TreeSet<Calendar> shutdownTimes = new TreeSet<>();
    /* 25 */ protected ArrayList<Integer> warnTimes = new ArrayList<>();
    /*     */
    /* 27 */ SettingsManager settings = SettingsManager.getInstance();

    /*     */
    /*     */ public void onDisable() {
        /* 30 */ this.shutdownImminent = false;
        /*     */
        /* 32 */ if (this.backgroundTimer != null) {
            /* 33 */ this.backgroundTimer.cancel();
            /* 34 */ this.backgroundTimer.purge();
            /* 35 */ this.backgroundTimer = null;
            /*     */ }
        /*     */
        /* 38 */ if (this.shutdownTimer != null) {
            /* 39 */ this.shutdownTimer.cancel();
            /* 40 */ this.shutdownTimer.purge();
            /* 41 */ this.shutdownTimer = null;
            /*     */ }
        /*     */
        /* 44 */ this.log.info("%s disabled.", new Object[] { this.settings.getDesc().getFullName() });
        /*     */ }

    /*     */
    /*     */ public void onEnable() {
        /* 48 */ this.pluginName = getDescription().getName();
        /* 49 */ this.log = new Log(this.pluginName);
        /*     */
        /* 51 */ this.settings.setup((Plugin) this);
        /*     */
        /* 53 */ this.scheduler = getServer().getScheduler();
        /* 54 */ this.shutdownImminent = false;
        /* 55 */ this.shutdownTimes.clear();
        /*     */
        /* 57 */ CommandExecutor autoShutdownCommandExecutor = new AutoShutdownCommand(this);
        /* 58 */ getCommand("autoshutdown").setExecutor(autoShutdownCommandExecutor);
        /* 59 */ getCommand("as").setExecutor(autoShutdownCommandExecutor);
        /*     */
        /* 61 */ scheduleAll();
        /*     */
        /* 63 */ Util.init((Plugin) this, this.log);
        /*     */
        /* 65 */ if (this.backgroundTimer != null) {
            /* 66 */ this.backgroundTimer.cancel();
            /* 67 */ this.backgroundTimer.purge();
            /* 68 */ this.backgroundTimer = null;
            /*     */ }
        /*     */
        /* 71 */ this.backgroundTimer = new Timer();
        /*     */
        /* 73 */ if (this.shutdownTimer != null) {
            /* 74 */ this.shutdownTimer.cancel();
            /* 75 */ this.shutdownTimer.purge();
            /* 76 */ this.shutdownTimer = null;
            /*     */ }
        /*     */
        /* 79 */ Calendar now = Calendar.getInstance();
        /* 80 */ now.set(13, 0);
        /* 81 */ now.add(12, 1);
        /*     */
        /* 83 */ now.add(14, 50);
        /*     */ try {
            /* 85 */ this.backgroundTimer.scheduleAtFixedRate(new ShutdownScheduleTask(this), now.getTime(), 60000L);
            /* 86 */ } catch (Exception e) {
            /* 87 */ this.log.severe("Failed to schedule AutoShutdownTask: %s", new Object[] { e.getMessage() });
            /*     */ }
        /*     */
        /* 90 */ this.log.info(String.valueOf(this.pluginName) + " enabled!");
        /*     */ }

    /*     */
    /*     */ protected void scheduleAll() {
        /* 94 */ this.shutdownTimes.clear();
        /* 95 */ this.warnTimes.clear();
        /*     */
        /* 97 */ String[] shutdownTimeStrings = null;
        /*     */
        /*     */ try {
            /* 100 */ shutdownTimeStrings = this.settings.getConfig().getString("times.shutdowntimes").split(",");
            /* 101 */ } catch (Exception e) {
            /* 102 */ shutdownTimeStrings[0] = this.settings.getConfig().getString("times.shutdowntimes");
            /*     */ }
        try {
            /*     */ byte b;
            int i;
            String[] arrayOfString1;
            /* 105 */ for (i = (arrayOfString1 = shutdownTimeStrings).length, b = 0; b < i;) {
                String timeString = arrayOfString1[b];
                /* 106 */ Calendar calendar = scheduleShutdownTime(timeString);
                /*     */
                /*     */ b++;
            }
            /*     */
            /* 110 */ String[] strings = getConfig().getString("times.warntimes").split(",");
            String[] arrayOfString2;
            /* 111 */ for (int j = (arrayOfString2 = strings).length; i < j;) {
                String warnTime = arrayOfString2[i];
                /* 112 */ this.warnTimes.add(Integer.decode(warnTime));
                i++;
            }
            /* 113 */ } catch (Exception e) {
            /* 114 */ this.log.severe("Unable to configure Auto Shutdown using the configuration file.");
            /* 115 */ this.log.severe("Is the format of shutdowntimes correct? It should be only HH:MM.");
            /* 116 */ this.log.severe("Error: %s", new Object[] { e.getMessage() });
            /*     */ }
        /*     */ }

    /*     */
    /*     */ protected Calendar scheduleShutdownTime(String timeSpec) throws Exception {
        /* 121 */ if (timeSpec == null) {
            /* 122 */ return null;
            /*     */ }
        /* 124 */ if (timeSpec.matches("^now$")) {
            /* 125 */ Calendar calendar = Calendar.getInstance();
            /* 126 */ int secondsToWait = getConfig().getInt("times.gracetime", 20);
            /* 127 */ calendar.add(13, secondsToWait);
            /*     */
            /* 129 */ this.shutdownImminent = true;
            /* 130 */ this.shutdownTimer = new Timer();
            /*     */
            /* 132 */ for (Integer warnTime : this.warnTimes) {
                /* 133 */ long longWarnTime = warnTime.longValue() * 1000L;
                /*     */
                /* 135 */ if (longWarnTime <= (secondsToWait * 1000)) {
                    /* 136 */ this.shutdownTimer.schedule(new WarnTask(this, warnTime.longValue()),
                            (secondsToWait * 1000) -
                            /* 137 */ longWarnTime);
                    /*     */ }
                /*     */ }
            /*     */
            /*     */
            /* 142 */ this.shutdownTimer.schedule(new ShutdownTask(this), calendar.getTime());
            /* 143 */ Util.broadcast("The server has been scheduled for immediate shutdown.");
            /*     */
            /* 145 */ return calendar;
            /*     */ }
        /*     */
        /* 148 */ if (!timeSpec.matches("^[0-9]{1,2}:[0-9]{2}$")) {
            /* 149 */ throw new Exception("Incorrect time specification. The format is HH:MM in 24h time.");
            /*     */ }
        /*     */
        /* 152 */ Calendar now = Calendar.getInstance();
        /* 153 */ Calendar shutdownTime = Calendar.getInstance();
        /*     */
        /* 155 */ String[] timecomponent = timeSpec.split(":");
        /* 156 */ shutdownTime.set(11, Integer.valueOf(timecomponent[0]).intValue());
        /* 157 */ shutdownTime.set(12, Integer.valueOf(timecomponent[1]).intValue());
        /* 158 */ shutdownTime.set(13, 0);
        /* 159 */ shutdownTime.set(14, 0);
        /*     */
        /* 161 */ if (now.compareTo(shutdownTime) >= 0) {
            /* 162 */ shutdownTime.add(5, 1);
            /*     */ }
        /*     */
        /* 165 */ this.shutdownTimes.add(shutdownTime);
        /*     */
        /* 167 */ return shutdownTime;
        /*     */ }

    /*     */
    /*     */ public void kickAll() {
        /* 171 */ if (!getConfig().getBoolean("kickonshutdown", true)) {
            /*     */ return;
            /*     */ }
        /*     */
        /* 175 */ this.log.info("Kicking all players ...");
        /* 176 */ this.log.info(this.settings.getConfig().getString("messages.kickreason"));
        /*     */
        /* 178 */ Collection players = getServer().getOnlinePlayers();
        byte b;
        int i;
        /*     */ Object[] arrayOfPlayer1;
        /* 180 */ for (i = (arrayOfPlayer1 = players.toArray()).length, b = 0; b < i;) {
            Player player = (Player) arrayOfPlayer1[b];
            /* 181 */ this.log.info("Kicking player %s.", new Object[] { player.getName() });
            /* 182 */ player.kickPlayer(this.settings.config.getString("messages.kickreason"));
            /*     */ b++;
        }
        /*     */
        /*     */ }

    /*     */ public SettingsManager getSettings() {
        /* 187 */ return this.settings;
        /*     */ }
    /*     */ }

/*
 * Location: D:\New folder
 * (2)\AutoShutdown.jar!\net\stupendous\autoshutdown\AutoShutdownPlugin.class
 * Java compiler version: 7 (51.0) JD-Core Version: 1.1.3
 */