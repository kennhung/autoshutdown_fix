/*     */ package net.stupendous.autoshutdown;
/*     */ 
/*     */ import java.util.Calendar;
/*     */ import net.stupendous.autoshutdown.misc.Log;
/*     */ import net.stupendous.autoshutdown.misc.Util;
/*     */ import org.bukkit.command.Command;
/*     */ import org.bukkit.command.CommandExecutor;
/*     */ import org.bukkit.command.CommandSender;
/*     */ import org.bukkit.entity.Player;
/*     */ 
/*     */ 
/*     */ 
/*     */ public class AutoShutdownCommand
/*     */   implements CommandExecutor
/*     */ {
/*     */   private final AutoShutdownPlugin plugin;
/*     */   private Log log;
/*     */   
/*     */   public AutoShutdownCommand(AutoShutdownPlugin plugin) {
/*  20 */     this.plugin = plugin;
/*  21 */     this.log = plugin.log;
/*     */   } public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
/*     */     Calendar stopTime;
/*     */     String timeString;
/*  25 */     if (sender instanceof Player && !((Player)sender).hasPermission("autoshutdown.admin")) {
/*  26 */       Util.replyError(sender, "You don't have permission to use that command.");
/*  27 */       return true;
/*     */     } 
/*     */     
/*  30 */     if (args.length == 0) {
/*  31 */       args = new String[] { "HELP" };
/*     */     }
/*     */ 
/*     */ 
/*     */ 
/*     */     
/*  37 */     switch (SubCommand.toSubCommand(args[0].toUpperCase()).ordinal()) {
/*     */       case 0:
/*  39 */         Util.reply(sender, "AutoShutdown plugin help:");
/*  40 */         Util.reply(sender, " /%s help", new Object[] { command.getName() });
/*  41 */         Util.reply(sender, "     Shows this help page");
/*  42 */         Util.reply(sender, " /%s reload", new Object[] { command.getName() });
/*  43 */         Util.reply(sender, "     Reloads the configuration file");
/*  44 */         Util.reply(sender, " /%s cancel", new Object[] { command.getName() });
/*  45 */         Util.reply(sender, "     Cancels the currently executing shutdown");
/*  46 */         Util.reply(sender, " /%s set HH:MM:SS", new Object[] { command.getName() });
/*  47 */         Util.reply(sender, "     Sets a new scheduled shutdown time");
/*  48 */         Util.reply(sender, " /%s set now", new Object[] { command.getName() });
/*  49 */         Util.reply(sender, "     Orders the server to shutdown immediately");
/*  50 */         Util.reply(sender, " /%s list", new Object[] { command.getName() });
/*  51 */         Util.reply(sender, "     lists the currently scheduled shutdowns");
/*     */         break;
/*     */       case 1:
/*  54 */         Util.reply(sender, "Reloading...");
/*  55 */         this.plugin.settings.reloadConfig();
/*  56 */         this.plugin.scheduleAll();
/*  57 */         Util.reply(sender, "Configuration reloaded.");
/*     */         break;
/*     */       case 2:
/*  60 */         if (this.plugin.shutdownTimer != null) {
/*  61 */           this.plugin.shutdownTimer.cancel();
/*  62 */           this.plugin.shutdownTimer.purge();
/*  63 */           this.plugin.shutdownTimer = null;
/*  64 */           this.plugin.shutdownImminent = false;
/*     */           
/*  66 */           Util.broadcast("Shutdown was aborted."); break;
/*     */         } 
/*  68 */         Util.replyError(sender, "There is no impending shutdown. If you wish to remove");
/*  69 */         Util.replyError(sender, "a scheduled shutdown, remove it from the configuration");
/*  70 */         Util.replyError(sender, "and reload.");
/*     */         break;
/*     */       
/*     */       case 3:
/*  74 */         if (args.length < 2) {
/*  75 */           Util.replyError(sender, "Usage:");
/*  76 */           Util.replyError(sender, "   /as set <time>");
/*  77 */           Util.replyError(sender, "<time> can be either 'now' or a 24h time in HH:MM format.");
/*  78 */           return true;
/*     */         } 
/*     */         
/*  81 */         stopTime = null;
/*     */         try {
/*  83 */           stopTime = this.plugin.scheduleShutdownTime(args[1]);
/*  84 */         } catch (Exception e) {
/*  85 */           Util.replyError(sender, "Usage:");
/*  86 */           Util.replyError(sender, "   /as set <time>");
/*  87 */           Util.replyError(sender, "<time> can be either 'now' or a 24h time in HH:MM format.");
/*     */         } 
/*  89 */         if (stopTime != null) {
/*  90 */           Util.reply(sender, "Shutdown scheduled for %s", new Object[] { stopTime.getTime().toString() });
/*     */         }
/*  92 */         timeString = "";
/*     */         
/*  94 */         for (Calendar shutdownTime : this.plugin.shutdownTimes) {
/*  95 */           if (((Calendar)this.plugin.shutdownTimes.first()).equals(shutdownTime)) {
/*  96 */             timeString = timeString
/*  97 */               .concat(String.format("%d:%02d", new Object[] { Integer.valueOf(shutdownTime.get(11)), 
/*  98 */                     Integer.valueOf(shutdownTime.get(12)) })); continue;
/*     */           } 
/* 100 */           timeString = timeString
/* 101 */             .concat(String.format(",%d:%02d", new Object[] { Integer.valueOf(shutdownTime.get(11)), 
/* 102 */                   Integer.valueOf(shutdownTime.get(12)) }));
/*     */         } 
/*     */ 
/*     */         
/* 106 */         this.plugin.settings.getConfig().set("times.shutdowntimes", timeString);
/*     */         break;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */       
/*     */       case 4:
/* 115 */         if (this.plugin.shutdownTimes.size() != 0) {
/* 116 */           Util.reply(sender, "Shutdowns scheduled at");
/* 117 */           for (Calendar shutdownTime : this.plugin.shutdownTimes) {
/* 118 */             Util.reply(sender, "   %s", new Object[] { shutdownTime.getTime().toString() });
/*     */           }  break;
/* 120 */         }  Util.replyError(sender, "No shutdowns scheduled.");
/*     */         break;
/*     */       
/*     */       case 5:
/* 124 */         Util.replyError(sender, "Unknown command. Use /as help to list available commands.");
/*     */         break;
/*     */     } 
/* 127 */     return true;
/*     */   }
/*     */   
/*     */   enum SubCommand {
/* 131 */     HELP, RELOAD, CANCEL, SET, LIST, UNKNOWN;
/*     */     
/*     */     private static SubCommand toSubCommand(String str) {
/*     */       try {
/* 135 */         return valueOf(str);
/* 136 */       } catch (Exception exception) {
/*     */         
/* 138 */         return HELP;
/*     */       } 
/*     */     }
/*     */   }
/*     */ }


/* Location:              D:\New folder (2)\AutoShutdown.jar!\net\stupendous\autoshutdown\AutoShutdownCommand.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       1.1.3
 */