/*    */ package net.stupendous.autoshutdown;
/*    */ 
/*    */ import java.util.Calendar;
/*    */ import java.util.Timer;
/*    */ import java.util.TimerTask;
/*    */ import net.stupendous.autoshutdown.misc.Util;
/*    */ import org.bukkit.plugin.Plugin;
/*    */ 
/*    */ public class ShutdownScheduleTask extends TimerTask {
/* 10 */   protected AutoShutdownPlugin plugin = null;
/*    */   
/*    */   ShutdownScheduleTask(AutoShutdownPlugin instance) {
/* 13 */     this.plugin = instance;
/*    */   }
/*    */   
/*    */   public void run() {
/* 17 */     this.plugin.getServer().getScheduler().scheduleSyncDelayedTask((Plugin)this.plugin, new Runnable() {
/*    */           public void run() {
/* 19 */             ShutdownScheduleTask.this.runTask();
/*    */           }
/*    */         });
/*    */   }
/*    */   
/*    */   private void runTask() {
/* 25 */     if (this.plugin.shutdownImminent) {
/*    */       return;
/*    */     }
/* 28 */     Calendar now = Calendar.getInstance();
/*    */     
/* 30 */     long firstWarning = (((Integer)this.plugin.warnTimes.get(0)).intValue() * 1000);
/*    */     
/* 32 */     for (Calendar cal : this.plugin.shutdownTimes) {
/* 33 */       if (cal.getTimeInMillis() - now.getTimeInMillis() <= firstWarning) {
/* 34 */         this.plugin.shutdownImminent = true;
/* 35 */         this.plugin.shutdownTimer = new Timer();
/*    */         
/* 37 */         for (Integer warnTime : this.plugin.warnTimes) {
/* 38 */           long longWarnTime = warnTime.longValue() * 1000L;
/*    */           
/* 40 */           if (longWarnTime <= cal.getTimeInMillis() - now.getTimeInMillis()) {
/* 41 */             this.plugin.shutdownTimer.schedule(new WarnTask(this.plugin, warnTime.longValue()), cal.getTimeInMillis() - 
/* 42 */                 now.getTimeInMillis() - longWarnTime);
/*    */           }
/*    */         } 
/*    */ 
/*    */         
/* 47 */         this.plugin.shutdownTimer.schedule(new ShutdownTask(this.plugin), cal.getTime());
/*    */         
/* 49 */         Util.broadcast(String.valueOf(this.plugin.settings.config.getString("messages.shutdownmessage")) + " at %s", 
/* 50 */             new Object[] { cal.getTime().toString() });
/*    */         break;
/*    */       } 
/*    */     } 
/*    */   }
/*    */ }


/* Location:              D:\New folder (2)\AutoShutdown.jar!\net\stupendous\autoshutdown\ShutdownScheduleTask.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       1.1.3
 */