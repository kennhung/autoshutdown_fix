/*    */ package net.stupendous.autoshutdown.misc;
/*    */ 
/*    */ import java.util.logging.Logger;
/*    */ 
/*    */ public class Log {
/*    */   private final Logger log;
/*    */   private final String pluginName;
/*    */   
/*    */   public Log(String pluginName) {
/* 10 */     this.pluginName = pluginName;
/* 11 */     this.log = Logger.getLogger("Minecraft." + pluginName);
/*    */   }
/*    */   
/*    */   public void info(String msg) {
/* 15 */     this.log.info(String.format("[%s] %s", new Object[] { this.pluginName, msg }));
/*    */   }
/*    */   
/*    */   public void info(String format, Object[] args) {
/* 19 */     info(String.format(format, args));
/*    */   }
/*    */   
/*    */   public void warning(String msg) {
/* 23 */     this.log.warning(String.format("[%s] %s", new Object[] { this.pluginName, msg }));
/*    */   }
/*    */   
/*    */   public void warning(String format, Object[] args) {
/* 27 */     warning(String.format(format, args));
/*    */   }
/*    */   
/*    */   public void severe(String msg) {
/* 31 */     this.log.severe(String.format("[%s] %s", new Object[] { this.pluginName, msg }));
/*    */   }
/*    */   
/*    */   public void severe(String format, Object[] args) {
/* 35 */     severe(String.format(format, args));
/*    */   }
/*    */ }


/* Location:              D:\New folder (2)\AutoShutdown.jar!\net\stupendous\autoshutdown\misc\Log.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       1.1.3
 */