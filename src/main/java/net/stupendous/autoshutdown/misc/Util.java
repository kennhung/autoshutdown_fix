/*     */ package net.stupendous.autoshutdown.misc;
/*     */ 
/*     */ import java.util.regex.Matcher;
/*     */ import java.util.regex.Pattern;
/*     */ import net.stupendous.autoshutdown.AutoShutdownPlugin;
/*     */ import org.bukkit.ChatColor;
/*     */ import org.bukkit.command.CommandSender;
/*     */ import org.bukkit.plugin.Plugin;
/*     */ 
/*     */ 
/*     */ public class Util
/*     */ {
/*     */   public static String pluginName;
/*  14 */   public static Log log = null;
/*  15 */   public static AutoShutdownPlugin plugin = null;
/*     */   
/*     */   public static void init(Plugin plugin, Log log) {
/*  18 */     Util.plugin = (AutoShutdownPlugin)plugin;
/*  19 */     pluginName = plugin.getDescription().getName();
/*  20 */     Util.log = log;
/*     */   }
/*     */   
/*     */   public static String parseColor(String s) {
/*  24 */     return parseColor("%s", new Object[] { s });
/*     */   }
/*     */   
/*     */   public static String parseColor(String format, Object[] args) {
/*  28 */     String pre = String.format(format, args);
/*  29 */     StringBuffer post = new StringBuffer();
/*     */     
/*  31 */     Pattern p = Pattern.compile("\\&([0-9a-fA-F&])");
/*  32 */     Matcher m = p.matcher(pre);
/*     */     
/*  34 */     while (m.find()) {
/*  35 */       String color; if (m.group(1).equalsIgnoreCase("&")) {
/*  36 */         m.appendReplacement(post, "&"); continue;
/*     */       } 
/*  38 */       int colorCode = Integer.parseInt(m.group(1), 16);
/*     */       
/*  40 */       switch (colorCode) {
/*     */         case 0:
/*  42 */           color = ChatColor.BLACK.toString();
/*     */           break;
/*     */         case 1:
/*  45 */           color = ChatColor.DARK_BLUE.toString();
/*     */           break;
/*     */         case 2:
/*  48 */           color = ChatColor.DARK_GREEN.toString();
/*     */           break;
/*     */         case 3:
/*  51 */           color = ChatColor.DARK_AQUA.toString();
/*     */           break;
/*     */         case 4:
/*  54 */           color = ChatColor.DARK_RED.toString();
/*     */           break;
/*     */         case 5:
/*  57 */           color = ChatColor.DARK_PURPLE.toString();
/*     */           break;
/*     */         case 6:
/*  60 */           color = ChatColor.GOLD.toString();
/*     */           break;
/*     */         case 7:
/*  63 */           color = ChatColor.GRAY.toString();
/*     */           break;
/*     */         case 8:
/*  66 */           color = ChatColor.DARK_GRAY.toString();
/*     */           break;
/*     */         case 9:
/*  69 */           color = ChatColor.BLUE.toString();
/*     */           break;
/*     */         case 10:
/*  72 */           color = ChatColor.GREEN.toString();
/*     */           break;
/*     */         case 11:
/*  75 */           color = ChatColor.AQUA.toString();
/*     */           break;
/*     */         case 12:
/*  78 */           color = ChatColor.RED.toString();
/*     */           break;
/*     */         case 13:
/*  81 */           color = ChatColor.LIGHT_PURPLE.toString();
/*     */           break;
/*     */         case 14:
/*  84 */           color = ChatColor.YELLOW.toString();
/*     */           break;
/*     */         case 15:
/*  87 */           color = ChatColor.WHITE.toString();
/*     */           break;
/*     */         default:
/*  90 */           color = ChatColor.WHITE.toString(); break;
/*     */       } 
/*  92 */       m.appendReplacement(post, color);
/*     */     } 
/*     */ 
/*     */     
/*  96 */     m.appendTail(post);
/*     */     
/*  98 */     return post.toString();
/*     */   }
/*     */   
/*     */   public static void replyError(CommandSender sender, String s) {
/* 102 */     replyError(sender, "%s", new Object[] { s });
/*     */   }
/*     */   
/*     */   public static void replyError(CommandSender sender, String format, Object[] args) {
/* 106 */     String msg = String.format(format, args);
/* 107 */     String formattedMessage = parseColor("&2[&a%s&2] &c%s", new Object[] { pluginName, msg });
/*     */     
/* 109 */     if (sender == null) {
/* 110 */       log.info(formattedMessage);
/*     */     } else {
/* 112 */       sender.sendMessage(formattedMessage);
/*     */     } 
/*     */   }
/*     */   public static void reply(CommandSender sender, String s) {
/* 116 */     reply(sender, "%s", new Object[] { s });
/*     */   }
/*     */   
/*     */   public static void reply(CommandSender sender, String format, Object[] args) {
/* 120 */     String msg = String.format(format, args);
/* 121 */     String formattedMessage = parseColor("&2[&a%s&2] &f%s", new Object[] { pluginName, msg });
/*     */     
/* 123 */     if (sender == null) {
/* 124 */       log.info(formattedMessage);
/*     */     } else {
/* 126 */       sender.sendMessage(formattedMessage);
/*     */     } 
/*     */   }
/*     */   public static String getJoinedStrings(String[] args, int initialIndex) {
/* 130 */     StringBuilder buffer = new StringBuilder();
/* 131 */     for (int i = initialIndex; i < args.length; i++) {
/* 132 */       if (i != initialIndex) {
/* 133 */         buffer.append(" ");
/*     */       }
/* 135 */       buffer.append(args[i]);
/*     */     } 
/* 137 */     return buffer.toString();
/*     */   }
/*     */   
/*     */   public static void broadcast(String s) {
/* 141 */     broadcast("%s", new Object[] { s });
/*     */   }
/*     */   
/*     */   public static void broadcast(String format, Object[] args) {
/* 145 */     String formattedMessage, msg = String.format(format, args);
/*     */     
/* 147 */     if (plugin.getSettings().getConfig().getBoolean("messages.showtag", true)) {
/* 148 */       formattedMessage = parseColor("&2[&a%s&2] &f%s", new Object[] { pluginName, msg });
/*     */     } else {
/* 150 */       formattedMessage = parseColor("&2[&a%s&2] &f%s", new Object[] { "Warning", msg });
/*     */     } 
/*     */     
/* 153 */     plugin.getServer().broadcastMessage(formattedMessage);
/*     */   }
/*     */ }


/* Location:              D:\New folder (2)\AutoShutdown.jar!\net\stupendous\autoshutdown\misc\Util.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       1.1.3
 */