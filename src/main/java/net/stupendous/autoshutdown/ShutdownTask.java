/*    */ package net.stupendous.autoshutdown;
/*    */ 
/*    */ import java.util.TimerTask;
/*    */ import net.stupendous.autoshutdown.misc.Log;
/*    */ import org.bukkit.Server;
/*    */ import org.bukkit.World;
/*    */ import org.bukkit.plugin.Plugin;
/*    */ 
/*    */ public class ShutdownTask
/*    */   extends TimerTask {
/* 11 */   protected AutoShutdownPlugin plugin = null;
/* 12 */   protected Log log = null;
/*    */   
/*    */   ShutdownTask(AutoShutdownPlugin instance) {
/* 15 */     this.plugin = instance;
/* 16 */     this.log = this.plugin.log;
/*    */   }
/*    */   
/*    */   public void run() {
/* 20 */     this.plugin.getServer().getScheduler().scheduleSyncDelayedTask((Plugin)this.plugin, new Runnable() {
/*    */           public void run() {
/* 22 */             ShutdownTask.this.log.info("Shutdown in progress.");
/*    */             
/* 24 */             ShutdownTask.this.plugin.kickAll();
/*    */             
/* 26 */             ShutdownTask.this.plugin.getServer().savePlayers();
/* 27 */             Server server = ShutdownTask.this.plugin.getServer();
/*    */             
/* 29 */             server.savePlayers();
/*    */             
/* 31 */             for (World world : server.getWorlds()) {
/* 32 */               world.save();
/* 33 */               server.unloadWorld(world, true);
/*    */             } 
/*    */             
/* 36 */             server.shutdown();
/*    */           }
/*    */         });
/*    */   }
/*    */ }


/* Location:              D:\New folder (2)\AutoShutdown.jar!\net\stupendous\autoshutdown\ShutdownTask.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       1.1.3
 */